import React, { Component } from "react";

class Cells extends Component {

    players(valCell) {

        Cell = (props) => {
            const { value, columnIndex, play } = props;
            const color = getColor(value);

        };
        if (valCell === 'b') {
            {/*blue */ }
            return 'blueCoin'
        } else if (valCell === 'y') {
            {/*yellow */ }
            return 'yellowCoin'
        } else {
            return;
        }
    }
    render() {
        return (
            <td>
                <div className={"cell " + (this.players(this.props.valCell) || 'empty')} onClick={() => this.props.onClick()}>
                    <div className={color}></div>
                </div>
            </td>
        );
    }
};

export default Cells;

{/* 
Cell = (props) => {
    const { value, columnIndex, play } = props;
    const color = getColor(value);
  
    return (
      <td>
        <div className="cell" onClick={() => {play(columnIndex)}}>
          <div className={ color }></div>
        </div>
      </td>
    );
  };*/}
