import React, { Component } from 'react';
import Cells from './Component/Cells';
import './App.css';
import Board from './Component/Board';

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Cells />
        <Board />
      </React.Fragment>

    );
  }
}

export default App;
